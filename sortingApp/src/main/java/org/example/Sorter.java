package org.example;

import java.util.*;

/**
 * A sorter class that sort an Integer list
 * The class contains the sort method and the sorter's constructor
 */

public class Sorter {
    public Sorter(){

    }
    /**
     Sort method
     @param  numbers an Integer List
     @return numbers sorted
    * */
    public Integer[] sort(Integer [] numbers){
        if(numbers==null){
            throw new NullPointerException();
        }
        if(numbers.length > 10){
            throw new IllegalArgumentException();
        }
        for(Integer i: numbers){
            if(i==null){
                throw new NullPointerException();
            }
        }
        Arrays.sort(numbers);
        return numbers;
    }
}
