package org.example;

import org.example.TestCases.DefaultTestCases;
import org.example.TestCases.NotValidTestCases;
import org.example.TestCases.NullTestCases;
import org.junit.Test;
import org.junit.runner.JUnitCore;
import org.junit.runner.Result;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class Tests {
    @Test
    public void testSuccessfulTestCases() {
        JUnitCore junit = new JUnitCore();
        Result result = junit.run(DefaultTestCases.class);

        assertEquals(0, result.getFailureCount());
        assertTrue("There must be at least 4 cases for parametrized tests", result.getRunCount() >= 4);
    }
    @Test
    public void testNullTestCases() {
        JUnitCore junit = new JUnitCore();
        Result result = junit.run(NullTestCases.class);

        assertEquals(0  , result.getFailureCount());
        assertTrue("There must be at least 4 cases for parametrized tests", result.getRunCount() >= 4);
    }
    @Test
    public void testNotValidCases(){
        JUnitCore junit = new JUnitCore();
        Result result = junit.run(NotValidTestCases.class);

        assertEquals(0  , result.getFailureCount());
        assertTrue("There must be at least 4 cases for parametrized tests", result.getRunCount() >= 4);
    }


}
