package org.example.TestCases;

import org.example.Sorter;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.assertTrue;
/*
    A class of tests where all must be run without errors
 */
@RunWith(Parameterized.class)

public class DefaultTestCases {

    protected Sorter sorter = new Sorter();;
    public Integer[] numbers;
    public Integer[] expected;

    /**
        @param numbers original list of Integers
        @param expected expected list after the sort method
     */
    public DefaultTestCases(Integer[] numbers, Integer [] expected) {
        this.expected = expected;
        this.numbers = numbers;
    }

    @Parameterized.Parameters
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][] {
                {  new Integer[]{0, 1, 4, 5, 3},new Integer[]{0, 1, 3, 4, 5}},
                {new Integer[]{0, 0, 0}, new Integer[]{0, 0, 0}},
                {new Integer[]{-400, 2, 8, 5, 5, 6, 4}, new Integer[]{-400, 2, 4, 5, 5, 6, 8}},
                {  new Integer[]{0, 1, 4, 5, 3, 2, 8, 9, 7, 6},new Integer[]{0, 1, 2, 3, 4, 5, 6, 7, 8, 9}},
                {new Integer[]{}, new Integer[]{}},
                {new Integer[]{1}, new Integer[]{1}}
        });
    }

    @Test
    public void testing(){

        assertTrue(Arrays.equals(expected, sorter.sort(numbers)));
    }
}
