package org.example.TestCases;

import org.example.Sorter;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.assertTrue;

/**
 * A test class to test some not valid tests
 */
@RunWith(Parameterized.class)
public class NotValidTestCases {
    protected Sorter sorter = new Sorter();
    public Integer[] numbers;

    /**
     @param numbers original list of Integers
     */
    public NotValidTestCases( Integer[] numbers) {
        this.numbers = numbers;
    }

    @Parameterized.Parameters
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][] {
                { new Integer[]{1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1}},
                { new Integer[]{0, 0, 0, 1, 1, 1, 2, 2, 2, 3, 3, 3 }},
                {new Integer[]{-400, 2, 8, 5, 5, 6, 4, 4, 52, 45, 45, 42}},
                {new Integer[]{0, 2, 1, 7, 4, 8, 45, 41, 41, 51, 41}}
        });
    }
    /**
     * All the tests that are not valid throw a IllegalArgumentException
     */
    @Test(expected = IllegalArgumentException.class)
    public void testing() {
        sorter.sort(numbers);
    }
}

