package org.example.TestCases;

import org.example.Sorter;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.assertThrows;
import static org.junit.Assert.assertTrue;
/**
 * A test class to verify that a case with nulls is not valid
 */
@RunWith(Parameterized.class)
public class NullTestCases {
    protected Sorter sorter = new Sorter();
    public Integer[] numbers;

    /**
     @param numbers original list of Integers
     */
    public NullTestCases(Integer[] numbers) {
        this.numbers = numbers;
    }

    @Parameterized.Parameters
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][] {
                {null},
                {new Integer[]{null, 5, 4}},
                {null},
                {new Integer[]{null, null, null}},
        });
    }

    /**
     * All the tests that contains a null value in the list throws a nullPointerException
     */
    @Test(expected = NullPointerException.class)
    public void testSortWithNulls() {
        sorter.sort(numbers);
    }

}